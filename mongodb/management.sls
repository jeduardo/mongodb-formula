{%- from 'mongodb/map.jinja' import mdb with context -%}

install pymongo package:
  pkg.installed:
    - name: python-pymongo

# Collect admin user information
{%- set ctx = {} %}
{%- for user, properties in mdb.users.items() %}
{%- if user == 'admin' %}
{%- for prop in properties %}
{%- if 'passwd' in prop.keys() %}
{%- do ctx.update({'mongodb_admin_pwd': prop['passwd']}) %}
{%- endif %}
{%- endfor %}
{%- endif%}
{%- endfor %}

{# Checking whether SSL is enabled in the server config #}
{%- set ssl = mdb.get('mongod_settings', {}).get('net', {}).get('ssl', {}).keys() %}

# Manage users
{%- for user, properties in mdb.users.items() %}
add user {{ user }}:
  {%- if 'absent' in properties %}
  {%- set action = 'absent' %}
  {%- else %}
  {%- set action = 'present' %}
  {%- endif %}
  mongodb_user.{{ action }}:
    - name: {{ user }}
    {{ properties | yaml(False) | indent(4) }}
    {%- if ssl %}
    - ssl: True
    {%- endif %}
    {%- if user != 'admin' %}
    # Details of the admin user for maintenance
    - user: admin
    - password: {{ ctx.mongodb_admin_pwd }}
    - authdb: admin
    {%- endif %}
    - require:
      {%- if user != 'admin' %}
      - mongodb_user: add user admin
      {%- endif %}
      - pkg: install pymongo package
{%- endfor %}
