# This setup for mongodb assumes that the replica set can be determined from
# the id of the minion

{%- from 'mongodb/map.jinja' import mdb with context -%}

{%- if mdb.use_repo %}

  {%- if grains['os_family'] == 'Debian' %}

    {%- set os   = salt['grains.get']('os') | lower() %}
    {%- set code = salt['grains.get']('oscodename') %}

mongodb_repo:
  pkgrepo.managed:
    - humanname: {{ mdb.repo_name }}
    - name: deb {{ mdb.repo_url }} {{ mdb.distro_name }} {{ mdb.repo_component }}
    - file: /etc/apt/sources.list.d/mongodb-org.list
    - keyid: {{ mdb.keyid }}
    - keyserver: {{ mdb.keyserver }}
    - clean_file: True

  {%- elif grains['os_family'] == 'RedHat' %}

mongodb_repo:
  pkgrepo.managed:
    {%- if mdb.version == 'stable' %}
    - name: mongodb-org
    - humanname: MongoDB.org Repository
    - gpgkey: https://www.mongodb.org/static/pgp/server-3.2.asc
    - baseurl: https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/{{ mdb.version }}/$basearch/
    - gpgcheck: 1
    {%- elif mdb.version == 'oldstable' %}
    - name: mongodb-org-{{ mdb.version }}
    - humanname: MongoDB oldstable Repository
    - baseurl: http://downloads-distro.mongodb.org/repo/redhat/os/$basearch/
    - gpgcheck: 0
    {%- else %}
    - name: mongodb-org-{{ mdb.version }}
    - humanname: MongoDB {{ mdb.version | capitalize() }} Repository
    - gpgkey: https://www.mongodb.org/static/pgp/server-{{ mdb.version }}.asc
    - baseurl: https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/{{ mdb.version }}/$basearch/
    - gpgcheck: 1
    {%- endif %}
    - disabled: 0
    - clean_file: True

  {%- endif %}

{%- endif %}

mongodb-group:
  group.present:
    - name: {{ mdb.mongodb_group }}
    - system: True

mongodb-user:
  {# ensure the parent directory of the home is here #}
  file.directory:
    - name: {{ mdb.db_path }}
    - makedirs: True
  user.present:
    - name: {{ mdb.mongodb_user }}
    - fullname: MongoDB Database User
    - home: {{ mdb.db_path }}
    - system: True
    - shell: /sbin/nologin
    - gid_from_name: rabbitmq
    {%- for property, value in mdb.get('user_properties', {}).items() %}
    - {{ property }}: {{ value }}
    {%- endfor %}
    - require:
      - group: mongodb-group
      - file: mongodb-user
    - watch_in:
      - service: mongodb_service
mongodb_package:
  # More recent mongodb packages are not reloading systemd after deploying
  # the mongod service. This causes a failure when trying to run the service
  # later on.
  {%- if grains['init'] == 'systemd' %}
  module.run:
    - name: service.systemctl_reload
    - require:
      - pkg: mongodb_package
  {%- endif %}
  pkg.installed:
    - name: {{ mdb.mongodb_package }}
    - require:
      - user: mongodb-user


mongodb_log_path:
  file.directory:
    {%- if 'mongod_settings' in mdb %}
    - name: {{ salt['file.dirname'](mdb.mongod_settings.systemLog.path) }}
    {%- else %}
    - name: {{ mdb.log_path }}
    {%- endif %}
    - user: {{ mdb.mongodb_user }}
    - group: {{ mdb.mongodb_group }}
    - mode: 755
    - makedirs: True
    - recurse:
      - user
      - group

mongodb_db_path:
  file.directory:
    {%- if 'mongod_settings' in mdb %}
    - name: {{ mdb.mongod_settings.storage.dbPath }}
    {%- else %}
    - name: {{ mdb.db_path }}
    {%- endif %}
    - user: {{ mdb.mongodb_user }}
    - group: {{ mdb.mongodb_group }}
    - mode: 755
    - makedirs: True
    - recurse:
      - user
      - group

mongodb_config:
  file.managed:
    - name: {{ mdb.conf_path }}
    - source: salt://mongodb/files/mongodb.conf.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644

{%- set replicaset_key = mdb.get('replicaset', {}).get('key', None) %}
{%- set replicaset_keyfile = mdb.get('replicaset', {}).get('keyfile', None) %}

{%- if replicaset_key %}
mongodb_keyfile:
  file.managed:
    - name: {{ replicaset_keyfile }}
    - makedirs: True
    - mode: 0400
    - dir_mode: 0400
    - user: {{ mdb.mongodb_user }}
    - group: {{ mdb.mongodb_group }}
    - contents: |
        {{ replicaset_key | indent(8) }}
{%- endif %}




{%- if mdb.use_percona %}
  {%- if mdb.use_ldap_auth %}

saslauthd_package:
    {%- if grains['init'] == 'systemd' %}
  module.run:
    - name: service.systemctl_reload
    - require:
      - pkg: sasl2-bin
    {%- endif %}
  pkg.installed:
    - name: sasl2-bin

fix_ldap_config:
  file.managed:
    - name: /etc/saslauthd.conf
    - user: root
    - group: root
    - mode: 644
    - contents: |
    {%- for property, value in mdb.get('ldap', {}).iteritems() %}
        {{ property }}: {{ value }}
    {%- endfor %}

fix_saslauthd_service:
  cmd.run:
    - name: sed -i 's/MECHANISMS="pam"/MECHANISMS="ldap"/g' /etc/default/saslauthd; sed -i 's/START=no/START=yes/g' /etc/default/saslauthd; systemctl daemon-reload

# Needed for MongoDB to connect to saslauthd
/etc/sasl2:
  file.directory:
    - user: mongod
    - group: mongod
    - mode: 755

sasl2_config:
  file.managed:
    - name: /etc/sasl2/mongodb.conf
    - user: mongod
    - group: mongod
    - mode: 644
    - contents: |
        pwcheck_method: saslauthd
        saslauthd_path: /var/run/saslauthd/mux
        log_level: 5
        mech_list: plain
# Manual restart needed as salt is not handling this service as it should

saslauthd_service:
  cmd.run:
    - name: systemctl restart saslauthd
    - watch: 
      - file: /etc/saslauthd.conf

fix_saslauthd_permissions:
  cmd.run:
    - name: chmod 777 /var/run/saslauthd

  {%- endif %}

fix_mongod_service:
  cmd.run:
    - name: sed -i 's/Type=forking/Type=simple/g' /lib/systemd/system/mongod.service && systemctl daemon-reload
    - onlyif: grep forking /lib/systemd/system/mongod.service
{%- endif %}

mongodb_service:
  service.running:
    - name: {{ mdb.mongod }}
    - enable: True
    - watch:
      - file: mongodb_config
      {%- if replicaset_keyfile %}
      - file: mongodb_keyfile
      {%- endif %}
{%- if replicaset_keyfile %}
# TODO: run this only if the node is intended as the master or initial master
# of the replicaset
mongodb_setup_replicaset:
  cmd.run:
    - name: mongo $(grep ssl /etc/mongod.conf | sed 's/\s+//g' | sed 's/://' | sed -rn 's/(\w+)/--\1 --sslAllowInvalidCertificates/p') --eval 'rs.initiate()'
    - onlyif: sleep 5 && mongo $(grep ssl /etc/mongod.conf | sed 's/\s+//g' | sed 's/://' | sed -rn 's/(\w+)/--\1 --sslAllowInvalidCertificates/p') --eval 'rs.status()' | grep NotYetInitialized
    - require:
      - service: mongodb_service
{%- endif %}


