mongodb:
  use_percona: False
  use_repo: True
  version: 3.6
  keyid: 58712A2291FA4AD5
  mongodb_package: mongodb-org
  conf_path: /etc/mongod.conf
  log_path: /srv/mongodb/log
  db_path: /srv/mongodb/data
  mongod: mongod
  user_properties:
    optional_groups:
      - ssl-cert
  replicaset:
    key: blablablatestkey
    keyfile: &keyfile /srv/mongodb/keyfile
  mongod_settings:
    systemLog:
      destination: file
      logAppend: true
      path: /var/log/mongodb/mongod.log
    storage:
      dbPath: /srv/mongodb/data
      journal:
        enabled: true
    net:
      port: 27017
      bindIp: 0.0.0.0

    setParameter:
      enableLocalhostAuthBypass: true
    security:
      authorization: enabled
      keyFile: *keyfile
    replication:
      replSetName: rs1
  users:
    admin:
      - check: False
      - passwd: password
      - roles:
        - db: admin
          role: root
    test:
      - passwd: testpassword
      - roles:
        - role: read
          db: reporting
        - role: readWrite
          db: test
