describe user('mongodb') do
  it { should exist }
end

describe group('mongodb') do
  it { should exist }
end

describe file('/var/log/mongodb') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/srv/mongodb') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/mongod.conf') do
 it { should exist }
 it { should be_owned_by 'root' }
end

describe service('mongod') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
