## For versions of Mongo that use the YAML format for configuration, use the
## following. All entries in mongod_settings are written to the config file
## verbatim. The storage:dbPath and systemLog:path entries are required in
## this usage and take precedence over db_path at the top level (see references
## in mongodb/init.sls).

## Use this for MongoDB 2.4
# mongodb:
#   use_repo: False
#   mongodb_package: mongodb
#   conf_path: /etc/mongodb.conf
#   db_path: /mongodb/data
#   log_path: /mongodb/log
#   settings:
#     log_append: True
#     bind_ip: 0.0.0.0
#     port: 27017
#     journal: True
#     configsvr: False
#     shardsvr: True
#     replSet: squiggles
#     rest: False
#     set_parameter:
#       textSearchEnabled: 'true'

## Use this for MongoDB 3.0 on Ubuntu
# mongodb:
#   version: 3.0
#   keyid: 7F0CEB10

## Use this for MongoDB 3.2
# mongodb:
#   use_repo: True
#   version: 3.2 # use oldstable in for 1.8 - 2.6
#   repo_component: multiverse    # this is for Ubuntu, use 'main' for Debian
#   mongodb_package: mongodb-org
#   mongodb_user: mongodb
#   mongodb_group: mongodb
#   mongod: mongod
#   conf_path: /etc/mongod.conf
#   log_path: /mongodb/log
#   db_path: /mongodb/data
#   mongod_settings:
#     systemLog:
#       destination: file
#       logAppend: true
#       path: /var/log/mongodb/mongod.log
#     storage:
#       dbPath: /var/lib/mongodb
#       journal:
#         enabled: true
#     net:
#       port: 27017
#       bindIp: 0.0.0.0
#     setParameter:
#       textSearchEnabled: true


## Use this for MongoDB 3.6 on Debian
mongodb:
  use_repo: True
  version: 3.6
  keyid: 58712A2291FA4AD5
  mongodb_package: mongodb-org
  conf_path: /etc/mongod.conf
  log_path: /srv/mongodb/log
  db_path: /srv/mongodb/data
  mongod: mongod
  # All properties here will be passed to the salt user state
  user_properties:
    optional_groups:
      - ssl-cert
  replicaset:
    key: blablablatestkey
    keyfile: &keyfile /srv/mongodb/keyfile
  mongod_settings:
    systemLog:
      destination: file
      logAppend: true
      path: /var/log/mongodb/mongod.log
    storage:
      dbPath: /srv/mongodb/data
      journal:
        enabled: true
    net:
      port: 27017
      bindIp: 0.0.0.0
      # This entire configuration block is required to enable TLS in the
      # MongoDB server
      #ssl:
      #  mode: requireSSL
      #  PEMKeyFile: /srv/certs/{{ grains['id'] }}.bundle.pem
      #  clusterFile: /srv/certs/{{ grains['id'] }}.client.bundle.pem
      #  CAFile: /srv/certs/bundleca.pem
      #  allowConnectionsWithoutCertificates: true
    setParameter:
      # This parameter allows the configuration of the admin user.
      # As soon as the admin user is configured, mongodb will no longer
      # allow this to happen.
      enableLocalhostAuthBypass: true
    security:
      authorization: enabled
      keyFile: *keyfile
    replication:
      replSetName: rs1
  users:
    # The admin user is special and it is provisioned before any other users.
    admin:
      # We don't want a user check to be executed for the admin user because
      # such a check requires the admin user to be present. Chicken and egg
      # problem.
      - check: False
      - passwd: password
      - roles:
        - db: admin
          role: root
    # All commands under the username are arguments for the mongodb_user state
    # and, as such, require a list format.
    test:
      - passwd: testpassword
      - roles:
        - role: read
          db: reporting
        - role: readWrite
          db: test


## Use this for Percona MongoDB Stable on Debian 
## To enable AD Authentication, set use_ldap_auth to True and set the 'database' parameter under the user node to be '$external' (check firstname.lastname)
## auditLog node sets the format and destination for audit logs

mongodb:
  use_percona: True
  use_ldap_auth: True
  version: stable
  log_path: /srv/mongodb/log
  db_path: /srv/mongodb/data
  user_properties:
    optional_groups:
      - ssl-cert
  replicaset:
    key: myverylongrskey
    keyfile: &keyfile /srv/mongodb/keyfile
  mongod_settings:
    systemLog:
      destination: file
      logAppend: true
      path: /var/log/mongodb/mongod.log
    storage:
      dbPath: /srv/mongodb/data
      journal:
        enabled: true
    net:
      port: 27017
      bindIp: 0.0.0.0
    auditLog:
      destination: file
      format: JSON
      # Make sure you set the audit file path to a directory the mongod user can write to,
      # otherwise the daemon will fail to start.
      path: /var/log/mongodb/audit.json
      # ssl:
      #        mode: requireSSL
      #    PEMKeyFile: /srv/certs/{{ grains['id'] }}.bundle.pem
      #   clusterFile: /srv/certs/{{ grains['id'] }}.client.bundle.pem
      #   CAFile: /srv/certs/bundleca.pem
      #  allowConnectionsWithoutCertificates: true
    setParameter:
      # This parameter allows the configuration of the admin user.
      # As soon as the admin user is configured, mongodb will no longer
      # allow this to happen.
      enableLocalhostAuthBypass: true
      authenticationMechanisms: PLAIN,SCRAM-SHA-1
    security:
      authorization: enabled
      keyFile: *keyfile
    replication:
      replSetName: rs1
  users:
    # The admin user is special and it is provisioned before any other users.
    admin:
      # We don't want a user check to be executed for the admin user because
      # such a check requires the admin user to be present. Chicken and egg
      # problem.
      - check: False
      - passwd: passwd
      - roles:
        - db: admin
          role: root
    mongo-backup:
      - passwd: passwd
      - roles:
        - db: admin
          role: backup
    firstname.lastname:
      - passwd: notneeded
      - database: "$external"
      - roles:
        - db: admin
          role: read
    # All commands under the username are arguments for the mongodb_user state
    # and, as such, require a list format.
  ldap:
    ldap_servers: ldap://adproxy.prod.infra.deposit
    ldap_search_base: DC=DS,DC=NET
    ldap_bind_dn: cn=LDAPUSER,OU=AD_Mgmt,OU=ServiceAccounts,DC=ds,DC=net
    ldap_password: LDAPPASSWORD
    ldap_filter: sAMAccountName=%u
    ldap_version: 3
    ldap_auth_metod: bind

